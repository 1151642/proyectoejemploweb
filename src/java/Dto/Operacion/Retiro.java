/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dto.Operacion;

import Dto.Cuenta;
import java.time.LocalDate;

/**
 *
 * @author dower
 */
public class Retiro extends Operacion{

    public Retiro() {
        super();
    }
    
    public void realizarOperacion(int retiro)
    {
        this.cuenta.setSaldo(this.cuenta.getSaldo()- retiro);
    }
    
}
