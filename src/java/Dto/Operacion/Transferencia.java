/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dto.Operacion;

import Dto.Cuenta;

/**
 *
 * @author dower
 */
public class Transferencia extends Operacion{

    public Transferencia() {
        super();
    }  
    
    public void realizarOperacion(Cuenta c, int transferencia)
    {
        this.cuenta.setSaldo(this.cuenta.getSaldo() - transferencia);
        c.setSaldo(c.getSaldo() + transferencia);
    }
    
}
