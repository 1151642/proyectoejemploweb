/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dto.Operacion;

import Dto.Cuenta;
import java.time.LocalDate;

/**
 *
 * @author dower
 */
public class Operacion {
    LocalDate fecha;
    double saldo;
    int identificador;
    public Cuenta cuenta;
    
    public Operacion() {
    }

    public Operacion(LocalDate fecha, double saldo, int identificador, Cuenta cuenta) {
        this.fecha = fecha;
        this.saldo = saldo;
        this.identificador = identificador;
        this.cuenta = cuenta;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public int getIdentificador() {
        return identificador;
    }

    public void setIdentificador(int identificador) {
        this.identificador = identificador;
    }
    
    
    
}
