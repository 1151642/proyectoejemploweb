
package Negocio;

import Dto.Operacion.Transferencia;
import Dto.Operacion.Consignacion;
import Dto.CuentaCorriente;
import Dto.*;
import Dto.Operacion.Retiro;
import java.time.LocalDate;
import java.util.TreeSet;

public class Banco {
    TreeSet<Cliente> clientes=new TreeSet();
    TreeSet<Cuenta> cuentas=new TreeSet();

    public Banco() {
    }
    
    public boolean insertarCliente(long cedula, String nombre, String dir, String fecha, String email, long telefono)
    {    
        Cliente nuevo=new Cliente();
        nuevo.setCedula(cedula);
        nuevo.setNombre(nombre);
        nuevo.setFechaNacimiento(crearFecha(fecha));
        nuevo.setDirCorrespondencia(dir);
        nuevo.setTelefono(telefono);
        nuevo.setEmail(email);
        //iría al dao , pero sólo por hoy en dinamic_memory
        return this.clientes.add(nuevo);
    }
    
    
    public boolean insertarCuenta(long nroCuenta, long cedula, boolean tipo)
    {
    Cliente x=new Cliente();
    x.setCedula(cedula);
    x=buscarCliente(x);
    if(x==null)    
        return false;
    Cuenta nueva=tipo?new CuentaAhorro():new CuentaCorriente();
    nueva.setCliente(x);
    nueva.setNroCuenta(nroCuenta);
    nueva.setFechaCreacion(LocalDate.now());
    return this.cuentas.add(nueva);
    }
    
    public boolean consignacionCuenta(long nroCuenta, int saldo)
    {
        Cuenta c = new Cuenta();
        c.setNroCuenta(nroCuenta);
        c = buscarCuenta(c);
        if(c==null)
            return false;
        Consignacion con = new Consignacion();
        con.cuenta = c;
        con.realizarOperacion(saldo);
        return true;
    }
    
    public boolean retiroCuenta(long nroCuenta, int saldo)
    {
        Cuenta c = new Cuenta();
        c.setNroCuenta(nroCuenta);
        c = buscarCuenta(c);
        if(c==null)
            return false;
        Retiro ret = new Retiro();
        ret.cuenta = c;
        ret.realizarOperacion(saldo);
        return true;
    }
    
    public boolean transferenciaCuenta(long nroCuenta, long nroCuentaTrans, int saldo)
    {
        Cuenta c = new Cuenta();
        c.setNroCuenta(nroCuenta);
        c = buscarCuenta(c);
        
        Cuenta trans = new Cuenta();
        trans.setNroCuenta(nroCuentaTrans);
        trans = buscarCuenta(trans);
        
        if(c==null || trans==null)
            return false;
        Transferencia tran = new Transferencia();
        tran.cuenta = c;
        tran.realizarOperacion(trans, saldo);
        return true;
    }
    
    private Cuenta buscarCuenta(Cuenta x)
    {
        if(this.cuentas.contains(x))
        {
            return this.cuentas.floor(x);
        }
        return null;
    }
    
    private Cliente buscarCliente(Cliente x)
    {
        if(this.clientes.contains(x))
        {
            return this.clientes.floor(x);
        }
        return null;
    }      
    
    private Cliente buscarCliente2(Cliente x)
    {
        for(Cliente y:this.clientes)
            if(y.equals(x))
                return y;
     return null;
    }
    
    
    private LocalDate crearFecha(String fecha)
    {
        String fechas[]=fecha.split("-");
        int agno=Integer.parseInt(fechas[0]);
        int mes=Integer.parseInt(fechas[1]);
        int dia=Integer.parseInt(fechas[2]);
        return LocalDate.of(agno,mes,dia);
    }

    public TreeSet<Cliente> getClientes() {
        return clientes;
    }
    
    public TreeSet<Cuenta> getCuentas(){
        return cuentas;
    }
    
    
    
}
